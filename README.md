# AIm Flouze App

AIm Flouze App (Analayseur d'Image de Flouze, Money Image Analyser in English) is an Flutter Application that works for Android.

The goal of the application is to allows the user to take and give picture to a REST Server (AIm Flouze Server, https://gitlab.com/p3490/aimflouzeserver). The REST Server will analyse the image, in search for european coins. The user can then retrieve the data from the REST Server in his application.

## How to install it?

First of all, ensure that flutter is install and functions correctly with the command :

```
flutter doctor
```

Then, you can run the application with the command

```
flutter pub get
flutter run -d [DEVICE]
```

## How to use it? (From the user point of view)

### Sign/Log In

First of all, you will be granted with a login page.

If don't have a account, that's the occasion to create one. You just need a email and a password to do so.

Otherwise, you can log in directly.

### Submit an analysis

From the main page, you will be able to see all previously submitted analyses. Right after your account creation, you won't be able to see much. You'll have to submit an analysis.

On the bottom-right hand corner of the page, you will see a "+" icon. If you click on it, you'll be able to choose the source of you analysis :

- A single photo taken from your gallery
- Take a picture directly from your camera. This will take two photos : One with the flash, one without.

In all cases, put all your pieces in a flat surface, with a signle distinctive color from the pieces.

After that, the application will submit the images to the REST server.

### The main page

You will go back then to the main page. On this page, you will be able to see all the previously submitted analysis. You can also do a lot of things in this page :

- Set the IP and port of the REST Server
- Set the user first and last name
- Sort the analysis list
- Filter all analysis based of various criterias
- Log off
- Update everything by scrolling the page from up to down

### The analysis report page

If you click on an analysis, you will be able to obtain the analysis detail page. On this page, you will be granted with a carousel.
This carousel contains all submitted pictures, pictures taken during development of the raport, and cropped pictures of each piece.

From this page, you can also delete the analysis.

## State of the project

### Developed functionalities

- Sign in and log in to the service
- List all the previously submitted analysis
- Sort and filter the analysis list
- View analysis detail
- Set up the account first and names
- Submit an anlysis with picture taken from the camera or from the user gallery
- Delete a preivously submitted analysis

### Knowns bugs (as of Jan 11, 2022)

- There might be some issue if the hour and date are wrongly set up on each device (the smartphone, and the server)
- After some actions, you have to manually update the main page
- The error showed in the bottom of the screen are sometimes not correctly retransmitted

### Ideas of upgrade

- Allow the user to note the result given to the user
- Add other filters and sort options based on the number of pieces, the total amount of money detected
- Improve the detail page UX design