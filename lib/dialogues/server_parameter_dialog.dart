import 'package:aim_flouze_app/utils/rest_utils.dart';
import 'package:flutter/material.dart';

/// A REST server parameter dialog Widget
class ServerParameterDialog extends StatefulWidget {
  const ServerParameterDialog({Key? key}) : super(key: key);

  @override
  _ServerParameterDialogState createState() => _ServerParameterDialogState();
}

class _ServerParameterDialogState extends State<ServerParameterDialog> {
  /// The TextEditingController containing the server IP/Name
  final TextEditingController _txtServer = TextEditingController();

  /// The TextEditingController containing the server port
  final TextEditingController _txtServerPort = TextEditingController();

  /// The RESTUtils utils
  static final RESTUtils _restUtils = RESTUtils();

  /// A global key associated to the form
  final GlobalKey<FormState> _formKey = GlobalKey();

  @override
  void initState() {
    _txtServer.text = _restUtils.serverName;
    _txtServerPort.text = _restUtils.serverPort;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text("Paramètres du serveur"),
      content: SingleChildScrollView(
        child: Form(
          child: Column(
            children: [
              // IP Entry
              TextFormField(
                  decoration: const InputDecoration(
                      labelText: "IP/Nom du serveur REST"),
                  controller: _txtServer,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Le nom du serveur est vide";
                    }
                    return null;
                  }),
              // Port Entry
              TextFormField(
                  decoration:
                      const InputDecoration(labelText: "Port du serveur REST"),
                  controller: _txtServerPort,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Le port du serveur est vide";
                    }
                    return null;
                  }),
              // Confirm button
              ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState?.validate() ?? false) {
                      _restUtils.serverName = _txtServer.text;
                      _restUtils.serverPort = _txtServerPort.text;
                      Navigator.pop(context);
                    }
                  },
                  child: const Text("Sauvegarder")),
            ],
          ),
          autovalidateMode: AutovalidateMode.onUserInteraction,
          key: _formKey,
        ),
      ),
    );
  }
}
