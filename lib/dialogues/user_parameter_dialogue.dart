import 'package:aim_flouze_app/dialogues/confirm_delete_account.dart';
import 'package:aim_flouze_app/models/user_informations.dart';
import 'package:aim_flouze_app/utils/rest_utils.dart';
import 'package:flutter/material.dart';

/// The user parameters dialog
class UsersParametersDialog extends StatefulWidget {
  final UserInformations? userInformations;

  const UsersParametersDialog(this.userInformations, {Key? key})
      : super(key: key);

  @override
  _UsersParametersDialogState createState() => _UsersParametersDialogState();
}

/// The User parameters dialog state
class _UsersParametersDialogState extends State<UsersParametersDialog> {
  /// The first name text editor
  final TextEditingController _firstNameEditor = TextEditingController();

  /// The last name text editor
  final TextEditingController _lastNameEditor = TextEditingController();

  /// Has the last update suceeded or not
  bool _lastError = false;

  @override
  void initState() {
    _firstNameEditor.text = widget.userInformations?.firstName ?? "";
    _lastNameEditor.text = widget.userInformations?.lastName ?? "";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text("Paramètres du compte"),
      content: SingleChildScrollView(
        child: Column(
          children: [
            TextField(
                decoration: const InputDecoration(labelText: "Prénom"),
                controller: _firstNameEditor),
            TextField(
                decoration: const InputDecoration(labelText: "Nom"),
                controller: _lastNameEditor),
            ElevatedButton(
                onPressed: () async {
                  setState(() {
                    _lastError = false;
                  });
                  final result = UserInformations(_firstName, _lastName);
                  final updateResponse =
                      await RESTUtils().updateUserInformation(result);
                  if (updateResponse.isOk) {
                    Navigator.pop(context);
                  } else {
                    setState(() {
                      _lastError = true;
                    });
                  }
                },
                child: const Text("Sauvegarder")),
            ElevatedButton(
                onPressed: () async {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => const ConfirmDeleteAccount()));
                },
                child: const Text("Supprimer le compte")),
            _bottomText
          ],
        ),
      ),
    );
  }

  /// Returns a widget that contains the error text, if needed,
  /// to put at the bottom
  Widget get _bottomText {
    if (_lastError) {
      return const Text("La mise à jour ne s'est pas bien effectué...");
    } else {
      return Container();
    }
  }

  /// Returns the first name set by the user
  String? get _firstName {
    return _firstNameEditor.text.isEmpty ? null : _firstNameEditor.text;
  }

  /// Returns the last name set by the user
  String? get _lastName {
    return _lastNameEditor.text.isEmpty ? null : _lastNameEditor.text;
  }
}
