import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

/// A generic Dialog widget, customizable, to confirm an action
/// The function returns null if the dialog action was refused
class ConfirmDialog<T> extends StatelessWidget {
  /// The dialog title
  final String title;

  /// The main text shown in the dialog widget
  final String mainText;

  /// The function launched after pushing the yes button
  final AsyncValueGetter<T> function;

  /// The text shown in the "yes button"
  final String yesButtonText;

  /// The text shown in the "no button"
  final String noButtonText;

  const ConfirmDialog(this.mainText, this.function,
      {Key? key,
      this.yesButtonText = "Oui",
      this.noButtonText = "Non",
      this.title = "Confirmation"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title),
      content: Text(mainText),
      actions: [
        ElevatedButton(
            onPressed: () async {
              _cleanClose(context, await function());
            },
            child: Text(yesButtonText)),
        ElevatedButton(
            onPressed: () {
              _cleanClose(context, null);
            },
            child: Text(noButtonText))
      ],
    );
  }

  /// Goes back to the previous page while returning a value
  void _cleanClose(BuildContext context, T? returnValue) {
    Navigator.pop(context, returnValue);
  }
}
