import 'package:aim_flouze_app/utils/rest_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

/// An AlertDialog used to confirm the deletion of an account
class ConfirmDeleteAccount extends StatefulWidget {
  const ConfirmDeleteAccount({Key? key}) : super(key: key);

  @override
  _ConfirmDeleteAccountState createState() => _ConfirmDeleteAccountState();
}

class _ConfirmDeleteAccountState extends State<ConfirmDeleteAccount> {
  /// The controller of the password form field
  final TextEditingController _mdpController = TextEditingController();

  /// The controller of the email form field
  final TextEditingController _emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text(
          "Par mesure de sécurité, pour supprimer votre compte, veuillez rentrer vos identifiants"),
      content: Column(
        children: [
          TextFormField(
              keyboardType: TextInputType.emailAddress,
              decoration: const InputDecoration(
                  hintText: "Adresse mail", icon: Icon(Icons.mail)),
              controller: _emailController),
          TextFormField(
            obscureText: true,
            controller: _mdpController,
            keyboardType: TextInputType.visiblePassword,
            decoration: const InputDecoration(
                hintText: "Mot de passe", icon: Icon(Icons.password)),
          )
        ],
      ),
      actions: [
        ElevatedButton(
            onPressed: () {
              RESTUtils()
                  .deleteAccount(_emailController.text, _mdpController.text)
                  .then((value) {
                if (value.isOk && value.content == true) {
                  Navigator.pushReplacementNamed(context, "/login");
                } else {
                  showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                            title: const Text("Erreur"),
                            content: const Text(
                                "Veillez essayez de vous déconnecter et de vous reconnecter"),
                            actions: [
                              ElevatedButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: const Text("OK"))
                            ],
                          ));
                }
              });
            },
            child: const Text("Supprimer")),
        ElevatedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text("Annuler"))
      ],
    );
  }
}
