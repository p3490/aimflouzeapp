import 'package:aim_flouze_app/models/submitted_analyse.dart';
import 'package:aim_flouze_app/utils/rest_utils.dart';
import 'package:aim_flouze_app/pages/analyse_details_page.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

/// A Widget containing a list of Submitted Analyses
class AnalyseList extends StatefulWidget {
  /// The List of SubmittedAnalyse to show in this widget
  final List<SubmittedAnalyse> analyseList;

  /// The function to launch when the user want to refresh the list
  final AsyncCallback refreshFunction;

  /// The RESTUtils
  static final RESTUtils rest = RESTUtils();

  const AnalyseList(this.analyseList, this.refreshFunction, {Key? key})
      : super(key: key);

  @override
  _AnalyseListState createState() => _AnalyseListState();
}

class _AnalyseListState extends State<AnalyseList> {
  /// The HTTP Headers to use to get the images
  Map<String, String>? httpHeaders;

  /// The DateFormat to show the analyse dates
  static final DateFormat dateFormat = DateFormat('dd/MM/yyyy HH:mm');

  @override
  void initState() {
    AnalyseList.rest.getDefaultHeaders().then((value) {
      setState(() {
        httpHeaders = value;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      child: ListView(
          children: widget.analyseList
              .map((analyse) => Card(
                  color: Colors.white,
                  elevation: 2.0,
                  child: ListTile(
                    title: Text(dateFormat.format(analyse.dateOfPost)),
                    subtitle: Text(analyse.stateDescription),
                    leading: (httpHeaders == null
                        ? null
                        : Image.network(analyse.defaultImage.pictureURL,
                            headers: httpHeaders)),
                    onTap: () {
                      Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (_) => AnalyseDetailPage(analyse)))
                          .then((value) {
                        widget.refreshFunction();
                      });
                    },
                  )))
              .toList()),
      onRefresh: widget.refreshFunction,
    );
  }
}
