import 'package:aim_flouze_app/models/submitted_analyse.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

/// A Widget describing a analysis result
class AnalyseTextDetail extends StatelessWidget {
  /// The format of the date
  static final DateFormat dateFormat = DateFormat("dd/MM/yyyy 'à' HH:mm");

  /// The analysis to resume
  final SubmittedAnalyse analyse;

  const AnalyseTextDetail(this.analyse, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextTheme textTheme = Theme.of(context).textTheme;
    return Column(children: [
      Center(
          child: Text("Analyse du ${dateFormat.format(analyse.dateOfPost)}",
              style: textTheme.headline5)),
      Padding(
        padding: const EdgeInsets.fromLTRB(5, 10, 5, 10),
        child: Text("Satistiques", style: textTheme.headline6),
      ),
      Padding(
          padding: const EdgeInsets.fromLTRB(5, 10, 5, 10),
          child: Text(
              """Nombre de pièces détectées : ${analyse.detectedPieces.length.toString()}
Somme détectée : ${analyse.summedValueDescription}
Pourcentage de certitude global : ${analyse.accuracyPeercent}""")),
    ], crossAxisAlignment: CrossAxisAlignment.start);
  }
}
