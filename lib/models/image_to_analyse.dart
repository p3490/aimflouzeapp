import 'package:camera/camera.dart';
import 'dart:convert';

/// An image to analyse
class ImageToAnalyse {
  /// The XFile of the image
  final XFile imageFile;

  /// Does the picture was taken with a flash?
  final bool hasFlash;

  const ImageToAnalyse(this.imageFile, this.hasFlash);

  /// Return a ImageToAnalyse from a JSON map
  factory ImageToAnalyse.fromJSON(Map<String, dynamic> json) {
    return ImageToAnalyse(
        XFile.fromData(base64Decode(json['imageFile'])), json['hasFlash']);
  }

  /// Convert the object to a self-representing JSON Map.
  /// The file is readen and encoded to base64.
  /// That's why the function is async.
  Future<Map<String, dynamic>> toJson() async {
    return {
      'imageFile': base64Encode(await imageFile.readAsBytes()),
      'hasFlash': hasFlash
    };
  }
}
