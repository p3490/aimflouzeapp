// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'submitted_analyse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SubmittedAnalyse _$SubmittedAnalyseFromJson(Map<String, dynamic> json) =>
    SubmittedAnalyse(
      json['uuid'] as String,
      json['user_id'] as String,
      DateTime.parse(json['date_of_post'] as String),
      (json['images'] as List<dynamic>)
          .map((e) => SubmittedImage.fromJson(e as Map<String, dynamic>))
          .toSet(),
      (json['pieces'] as List<dynamic>)
          .map((e) => PieceDetection.fromJson(e as Map<String, dynamic>))
          .toSet(),
      json['finished'] as bool,
      json['error'] as bool,
    );

Map<String, dynamic> _$SubmittedAnalyseToJson(SubmittedAnalyse instance) =>
    <String, dynamic>{
      'uuid': instance.uuid,
      'user_id': instance.userId,
      'date_of_post': instance.dateOfPost.toIso8601String(),
      'images': instance.submittedImages.toList(),
      'pieces': instance.detectedPieces.toList(),
      'finished': instance.finished,
      'error': instance.error,
    };
