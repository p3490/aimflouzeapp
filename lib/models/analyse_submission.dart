import 'package:aim_flouze_app/models/image_to_analyse.dart';

/// An analyse to submit
class AnalyseSubmission {
  /// A list of images to analyse for this submission
  final List<ImageToAnalyse> images;

  /// The datetime to which the photos were taken
  final DateTime date;

  const AnalyseSubmission(this.images, this.date);

  /// Read a JSON map representing and analyse submission and
  /// return a AnalyseSubmission with it.
  factory AnalyseSubmission.fromJson(Map<String, dynamic> json) {
    List<ImageToAnalyse> images = [];
    for (Map<String, dynamic> map in json['images']) {
      images.add(ImageToAnalyse.fromJSON(map));
    }
    return AnalyseSubmission(images, DateTime.parse(json['date']));
  }

  /// Transform this analyse submission into a JSON map.
  /// This function is asynchronous because the images are read and
  /// encoded in base64.
  Future<Map<String, dynamic>> toJson() async {
    List<Map<String, dynamic>> imagesToJson = [];
    for (var image in images) {
      imagesToJson.add(await image.toJson());
    }
    return {'date': date.toIso8601String(), 'images': imagesToJson};
  }
}
