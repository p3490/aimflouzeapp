typedef SortAlgorithm<T> = int Function(T e1, T e2);

/// A class representing a sotring option
class SortOption<T> {
  /// The name of this sort option
  final String name;

  /// The function that determins the order between two elements
  final SortAlgorithm<T>? sortAlgorithm;

  const SortOption(this.name, this.sortAlgorithm);
}
