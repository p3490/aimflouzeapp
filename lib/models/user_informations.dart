import 'package:json_annotation/json_annotation.dart';

part 'user_informations.g.dart';

/// A model representing the user information stocked in the database
@JsonSerializable()
class UserInformations {
  /// The first name of the user
  @JsonKey(name: "first_name")
  final String? firstName;

  /// The last name of the user
  @JsonKey(name: "last_name")
  final String? lastName;

  const UserInformations(this.firstName, this.lastName);

  /// Create a UserInformation from a JSON Map representing it
  factory UserInformations.fromJson(Map<String, dynamic> json) =>
      _$UserInformationsFromJson(json);

  /// Transform this UserInformation into a JSON Map representing it
  Map<String, dynamic> toJson() => _$UserInformationsToJson(this);
}
