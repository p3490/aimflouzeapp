import 'package:aim_flouze_app/models/submitted_analyse.dart';

/// A class representing a list of parameters to filter the analysis
class FilterParameters {
  /// From which date shall the analysis be kept?
  final DateTime? fromDate;

  /// Up to which shall the analysis be kept?
  final DateTime? toDate;

  /// The minimal amount of money accepted for a analysis, in cents
  final double? minValue;

  /// The maximal amount of money accepted for a analysis, in cents
  final double? maxValue;

  const FilterParameters(
      this.fromDate, this.toDate, this.minValue, this.maxValue);

  /// Returns an empty FilterParameters, that accepts everything
  const FilterParameters.empty() : this(null, null, null, null);

  /// Determins if a Submitted Analysis has to be kept or no.
  bool shouldBeKept(SubmittedAnalyse element) {
    return (fromDate?.isBefore(element.dateOfPost) ?? true) &&
        (toDate?.isAfter(element.dateOfPost) ?? true) &&
        ((minValue ?? element.summedValue) <= element.summedValue) &&
        ((maxValue ?? element.summedValue) >= element.summedValue);
  }
}
