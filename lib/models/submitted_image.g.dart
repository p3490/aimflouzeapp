// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'submitted_image.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SubmittedImage _$SubmittedImageFromJson(Map<String, dynamic> json) =>
    SubmittedImage(
      json['uuid'] as String,
      json['with_flash'] as bool,
      json['is_dev'] as bool,
    );

Map<String, dynamic> _$SubmittedImageToJson(SubmittedImage instance) =>
    <String, dynamic>{
      'uuid': instance.uuid,
      'with_flash': instance.hasFlash,
      'is_dev': instance.isDev,
    };
