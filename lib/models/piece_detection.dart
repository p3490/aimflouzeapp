import 'package:aim_flouze_app/utils/rest_utils.dart';
import 'package:json_annotation/json_annotation.dart';

part 'piece_detection.g.dart';

/// A piece detected in a image
@JsonSerializable()
class PieceDetection {
  /// The id of the piece
  @JsonKey()
  final int id;

  /// The value of the piece, in cents
  @JsonKey(name: "value_cents")
  final int value;

  /// The position of the piece in the x axis in the picture
  @JsonKey(name: "position_x")
  final int positionX;

  /// The position of the piece in the y axis in the picture
  @JsonKey(name: "position_y")
  final int positionY;

  /// The width of the piece in the picture
  @JsonKey()
  final int width;

  /// The height of the piece in the picture
  @JsonKey()
  final int height;

  /// A number from 0 to 1 representing the peercentage of certitude of the AI guess
  /// for this piece
  @JsonKey()
  final double accuracy;

  const PieceDetection(this.id, this.value, this.positionX, this.positionY,
      this.width, this.height, this.accuracy);

  /// From a JSON Map, returns a PieceDetection
  factory PieceDetection.fromJson(Map<String, dynamic> json) =>
      _$PieceDetectionFromJson(json);

  /// Transform a PieceDetection into a JSON Map
  Map<String, dynamic> toJson() => _$PieceDetectionToJson(this);

  /// Return an description of the coin value, in euro or cents
  String get valueDescription {
    if (value >= 100) {
      return "${value ~/ 100}€";
    } else {
      return "$value¢";
    }
  }

  /// Returns a accuracy peercentage, as a string
  String get accuracyPeercent {
    if (accuracy < 0.0001) {
      return "- de 0.01%";
    }
    return "${(accuracy * 100).toStringAsFixed(2)}%";
  }

  /// Returns the URL of the image associated to this piece
  String get imageURL {
    RESTUtils rest = RESTUtils();
    return "http://${rest.serverName}:${rest.serverPort}/pieces/$id/picture";
  }
}
