// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'piece_detection.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PieceDetection _$PieceDetectionFromJson(Map<String, dynamic> json) =>
    PieceDetection(
      json['id'] as int,
      json['value_cents'] as int,
      json['position_x'] as int,
      json['position_y'] as int,
      json['width'] as int,
      json['height'] as int,
      (json['accuracy'] as num).toDouble(),
    );

Map<String, dynamic> _$PieceDetectionToJson(PieceDetection instance) =>
    <String, dynamic>{
      'id': instance.id,
      'value_cents': instance.value,
      'position_x': instance.positionX,
      'position_y': instance.positionY,
      'width': instance.width,
      'height': instance.height,
      'accuracy': instance.accuracy,
    };
