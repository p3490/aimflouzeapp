import 'package:aim_flouze_app/utils/rest_utils.dart';
import 'package:json_annotation/json_annotation.dart';

part 'submitted_image.g.dart';

/// An SubmittedImage to the database
@JsonSerializable()
class SubmittedImage {
  /// The UUID of the image in the database
  @JsonKey()
  final String uuid;

  /// Does the picture was taken with a flash?
  @JsonKey(name: "with_flash")
  final bool hasFlash;

  /// Is this an image generated during development?
  @JsonKey(name: "is_dev")
  final bool isDev;

  const SubmittedImage(this.uuid, this.hasFlash, this.isDev);

  /// Returns the URL of the pictures
  String get pictureURL {
    RESTUtils rest = RESTUtils();
    return "http://${rest.serverName}:${rest.serverPort}/images/$uuid/picture";
  }

  /// From a JSON Map, returns a SubmittedImage
  factory SubmittedImage.fromJson(Map<String, dynamic> json) =>
      _$SubmittedImageFromJson(json);

  /// Transform a SubmittedImage into a JSON Map
  Map<String, dynamic> toJson() => _$SubmittedImageToJson(this);
}
