// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_informations.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserInformations _$UserInformationsFromJson(Map<String, dynamic> json) =>
    UserInformations(
      json['first_name'] as String?,
      json['last_name'] as String?,
    );

Map<String, dynamic> _$UserInformationsToJson(UserInformations instance) =>
    <String, dynamic>{
      'first_name': instance.firstName,
      'last_name': instance.lastName,
    };
