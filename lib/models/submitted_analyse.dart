import 'package:aim_flouze_app/models/submitted_image.dart';
import 'package:aim_flouze_app/models/piece_detection.dart';
import 'package:json_annotation/json_annotation.dart';

part 'submitted_analyse.g.dart';

/// An analyse allready submitted to the database
@JsonSerializable()
class SubmittedAnalyse {
  /// The analyse uuid
  @JsonKey()
  final String uuid;

  /// The id of the user responsible for this analyse
  @JsonKey(name: "user_id")
  final String userId;

  /// The date at whitch this analyse has been posted
  @JsonKey(name: "date_of_post")
  final DateTime dateOfPost;

  /// A Set containing all submitted images for this analyse
  @JsonKey(name: "images")
  final Set<SubmittedImage> submittedImages;

  /// A Set containing all detected pieces for this analyse
  @JsonKey(name: "pieces")
  final Set<PieceDetection> detectedPieces;

  /// Is the analyse done?
  @JsonKey()
  final bool finished;

  /// Was the analyse successfull?
  @JsonKey()
  final bool error;

  const SubmittedAnalyse(this.uuid, this.userId, this.dateOfPost,
      this.submittedImages, this.detectedPieces, this.finished, this.error);

  /// The value, in cents, of all the money summed up in the picture
  int get summedValue {
    int result = 0;
    for (var piece in detectedPieces) {
      result += piece.value;
    }
    return result;
  }

  /// Returns a String représentation of the summed value description
  String get summedValueDescription {
    int money = summedValue;
    int euros = money ~/ 100;
    int cents = money % 100;
    StringBuffer result = StringBuffer();
    if (euros > 0) {
      result.write(euros);
      if (cents > 0) {
        result.write(".");
        if (cents < 10) {
          result.write("0");
        }
      }
    }
    if (cents > 0 || euros == 0) {
      result.write(cents);
    }
    result.write(euros > 0 ? "€" : "¢");
    return result.toString();
  }

  /// A summed up accuracy note of this analyse, from 0 to 1
  double get totalAccuracy {
    double result = 0;
    int numberOfPieces = 0;
    for (var piece in detectedPieces) {
      result += piece.accuracy;
      numberOfPieces += 1;
    }
    return result / numberOfPieces;
  }

  /// Returns a accuracy peercentage
  String get accuracyPeercent {
    return "${(totalAccuracy * 100).toStringAsFixed(2)}%";
  }

  /// From a JSON Map, returns a SubmittedImage
  factory SubmittedAnalyse.fromJson(Map<String, dynamic> json) =>
      _$SubmittedAnalyseFromJson(json);

  /// Transform a SubmittedImage into a JSON Map
  Map<String, dynamic> toJson() => _$SubmittedAnalyseToJson(this);

  /// Returns a default image to represent this analyse
  SubmittedImage get defaultImage {
    return ordonatedImages[0];
  }

  /// Returns the image ordonated in a list.
  /// "True images" without flash will come first, then without flash,
  /// and at the end all the "developments images"
  List<SubmittedImage> get ordonatedImages {
    List<SubmittedImage> result = List.from(submittedImages);
    result.sort((a, b) {
      if (a.isDev != b.isDev) {
        return a.isDev ? 1 : -1;
      }
      if (a.hasFlash != b.hasFlash) {
        return a.hasFlash ? -1 : 1;
      } else {
        return a.uuid.compareTo(b.uuid);
      }
    });
    return result;
  }

  /// Returns all the pieces sorted by their value
  List<PieceDetection> get ordonatedPieces {
    List<PieceDetection> result = List.from(detectedPieces);
    result.sort((a, b) => a.value - b.value);
    return result;
  }

  /// Returns a String describing the analyis state
  String get stateDescription {
    if (error) {
      return "Erreur lors du traitement";
    } else if (finished) {
      return "Valeur détectée : $summedValueDescription";
    } else {
      return "En cours...";
    }
  }
}
