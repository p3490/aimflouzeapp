import 'package:aim_flouze_app/models/sort_algorithms.dart';
import 'package:aim_flouze_app/pages/connexion_page.dart';
import 'package:aim_flouze_app/pages/main_page.dart';
import 'package:aim_flouze_app/utils/rest_utils.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String initialRoute =
        (RESTUtils().firebaseUser == null ? "/login" : "/home");
    Widget mainPage = MainPage(sortOptions: [
      SortOption("Du plus récent au plus vieux",
          (a, b) => -a.dateOfPost.compareTo(b.dateOfPost)),
      SortOption("Du plus vieux au plus récent",
          (a, b) => a.dateOfPost.compareTo(b.dateOfPost)),
      SortOption("Par valeur croissante",
          (e1, e2) => e1.summedValue.compareTo(e2.summedValue)),
      SortOption("Par valeur décroissante",
          (e1, e2) => -e1.summedValue.compareTo(e2.summedValue))
    ]);
    return MaterialApp(
        title: "AImFlouzeApp",
        theme: ThemeData(
            primarySwatch: Colors.deepPurple,
            visualDensity: VisualDensity.adaptivePlatformDensity),
        initialRoute: initialRoute,
        routes: {
          '/login': (context) => const ConnexionPage(),
          '/home': (context) => mainPage
        });
  }
}
