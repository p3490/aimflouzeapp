import 'package:aim_flouze_app/models/analyse_submission.dart';
import 'package:aim_flouze_app/models/image_to_analyse.dart';
import 'package:aim_flouze_app/pages/upload_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

/// A page to confirm the upload of the images
class ConfirmUploadPage extends StatefulWidget {
  /// The list of images to confirm
  final List<ImageToAnalyse> images;

  const ConfirmUploadPage(this.images, {Key? key}) : super(key: key);

  @override
  _ConfirmUploadPageState createState() => _ConfirmUploadPageState();
}

class _ConfirmUploadPageState extends State<ConfirmUploadPage> {
  /// The list of widget (One for each image) to show in this page
  List<Widget> toShow = [];

  @override
  void initState() {
    for (var image in widget.images) {
      image.imageFile.readAsBytes().then((value) {
        setState(() {
          toShow.add(Expanded(child: Image.memory(value, fit: BoxFit.contain)));
        });
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar:
            AppBar(title: const Center(child: Text("Confirmation")), actions: [
          IconButton(
              onPressed: () {
                var analyse = AnalyseSubmission(widget.images, DateTime.now());
                SystemChrome.setPreferredOrientations([
                  DeviceOrientation.landscapeRight,
                  DeviceOrientation.landscapeLeft,
                  DeviceOrientation.portraitUp,
                  DeviceOrientation.portraitDown,
                ]);
                Navigator.push(context,
                    MaterialPageRoute(builder: (_) => UploadPage(analyse)));
              },
              icon: const Icon(Icons.check))
        ]),
        body: ConstrainedBox(
            child: Row(children: toShow),
            constraints:
                BoxConstraints(maxWidth: MediaQuery.of(context).size.width)));
  }
}
