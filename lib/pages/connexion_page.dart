import 'package:aim_flouze_app/dialogues/server_parameter_dialog.dart';
import 'package:aim_flouze_app/utils/rest_utils.dart';
import 'package:flutter/material.dart';

/// The connexion page widget, that allows the user to sign in and to register
class ConnexionPage extends StatefulWidget {
  const ConnexionPage({Key? key}) : super(key: key);

  @override
  _ConnexionPageState createState() => _ConnexionPageState();
}

class _ConnexionPageState extends State<ConnexionPage> {
  /// Does the user wants to connect?
  bool _isConnectable = true;

  /// The error message at the bottom of the widget
  String _message = "";

  /// The TextEditingController containing the user email
  final TextEditingController _txtEmail = TextEditingController();

  /// The TextEditingController containing the password
  final TextEditingController _txtMdp = TextEditingController();

  static final RESTUtils _restUtils = RESTUtils();

  /// Returns a widget that allows the user to enter his email
  Widget get _emailWidget {
    double height = MediaQuery.of(context).size.height;
    return Padding(
        padding: EdgeInsets.only(top: height / 10),
        child: TextFormField(
            keyboardType: TextInputType.emailAddress,
            decoration: const InputDecoration(
                hintText: "Adresse mail", icon: Icon(Icons.mail)),
            controller: _txtEmail));
  }

  /// Returns a widget that allows the user to enter his password
  Widget get _passwordWidget {
    double height = MediaQuery.of(context).size.height;
    return Padding(
        padding: EdgeInsets.only(top: height / 10),
        child: TextFormField(
            keyboardType: TextInputType.visiblePassword,
            obscureText: true,
            decoration: const InputDecoration(
                hintText: "Mot de passe", icon: Icon(Icons.password)),
            controller: _txtMdp));
  }

  /// Returns the main button of this widget (Sign in or register)
  Widget get _mainButton {
    String text = _isConnectable ? "Se connecter" : "S'inscrire";
    double height = MediaQuery.of(context).size.height;
    return Padding(
        child: ElevatedButton(
            onPressed: apply,
            child: Text(text),
            style: ButtonStyle(
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20)))))),
        padding: EdgeInsets.only(top: height / 10));
  }

  /// Returns the secondary button of this widget (Sign in or register)
  Widget get _secondaryButton {
    String text = _isConnectable ? "S'inscrire" : "Se connecter";
    return TextButton(
        onPressed: () {
          setState(() {
            _isConnectable = !_isConnectable;
          });
        },
        child: Text(text));
  }

  /// Return a validation message widget
  Widget get _validationMessageWidget {
    return Text(_message);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Center(child: Text("Connexion")),
          leading: null,
          automaticallyImplyLeading: false,
          actions: [
            IconButton(
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (context) => const ServerParameterDialog());
                },
                icon: const Icon(Icons.settings))
          ],
        ),
        body: SingleChildScrollView(
            child: Padding(
                child: Form(
                    child: Column(
                  children: [
                    _emailWidget,
                    _passwordWidget,
                    _mainButton,
                    _secondaryButton,
                    _validationMessageWidget
                  ],
                )),
                padding: const EdgeInsets.all(24))));
  }

  /// Sign in or register the user based on the widget state
  Future<void> apply() async {
    setState(() {
      _message = "";
    });
    try {
      String? userID;
      if (_isConnectable) {
        userID = await _restUtils.signIn(_txtEmail.text, _txtMdp.text);
      } else {
        var response =
            await _restUtils.createAccount(_txtEmail.text, _txtMdp.text);
        if (response.responseStatus != RESTResponseStatus.ok) {
          throw ErrorDescription("Connexion au serveur REST impossible");
        } else {
          userID = response.content;
        }
      }
      if (userID != null) {
        Navigator.pushReplacementNamed(context, "/home");
      } else {
        throw ErrorDescription("Server REST deficient!");
      }
    } catch (e) {
      setState(() {
        _message = e.toString();
      });
    }
  }
}
