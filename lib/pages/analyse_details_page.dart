import 'dart:typed_data';

import 'package:aim_flouze_app/dialogues/confirm_dialogue.dart';
import 'package:aim_flouze_app/models/piece_detection.dart';
import 'package:aim_flouze_app/models/submitted_analyse.dart';
import 'package:aim_flouze_app/models/submitted_image.dart';
import 'package:aim_flouze_app/utils/rest_utils.dart';
import 'package:aim_flouze_app/widgets/analyse_text_detail.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

/// A widget containing all details of a Analyse
class AnalyseDetailPage extends StatefulWidget {
  /// The analyse to detail
  final SubmittedAnalyse analyse;

  const AnalyseDetailPage(this.analyse, {Key? key}) : super(key: key);

  @override
  _AnalyseDetailPageState createState() => _AnalyseDetailPageState();
}

class _AnalyseDetailPageState extends State<AnalyseDetailPage> {
  /// A list of ImagesProvider for images to be shown
  List<ImageProvider> images = [];

  /// A list of text to show in this widget
  List<Widget> contentTexts = [];

  /// The index of the widget shown in CarouselSlider
  int _indexToShow = 0;

  @override
  void initState() {
    // Initialisation of the image with a default value
    images = List.filled(widget.analyse.submittedImages.length + _pieces.length,
        const AssetImage("assets/questionMark.png"));

    /// Updates the image list as soon as the headers are set
    RESTUtils().getDefaultHeaders().then((headers) async {
      List<SubmittedImage> submitedImages = widget.analyse.ordonatedImages;
      List<NetworkImage> mainImages = submitedImages
          .map((e) => NetworkImage(e.pictureURL, headers: headers))
          .toList();
      List<NetworkImage> piecesImages = _pieces
          .map((e) => NetworkImage(e.imageURL, headers: headers))
          .toList();
      setState(() {
        for (int i = 0; i < mainImages.length; i += 1) {
          images[i] = mainImages[i];
        }
        for (int i = 0; i < piecesImages.length; i += 1) {
          images[i + mainImages.length] = piecesImages[i];
        }
      });
    });

    // Initialisation of the descriptionlist
    // First, with the main text details
    Widget mainText = AnalyseTextDetail(widget.analyse);
    for (int i = 0; i < widget.analyse.submittedImages.length; i += 1) {
      contentTexts.add(mainText);
    }

    // Then, with a piece description
    for (var piece in _pieces) {
      contentTexts.add(Center(
          child: Padding(
              child: Text(
                  "Piece de ${piece.valueDescription} (${piece.accuracyPeercent})"),
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10))));
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Détails"), actions: [
          IconButton(
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (_) => ConfirmDialog(
                            "Voulez-vous supprimer cette analyse?", () async {
                          return (await RESTUtils()
                                  .deleteAnalyse(widget.analyse.uuid))
                              .isOk;
                        })).then((value) {
                  if (value != null && value == true) {
                    Navigator.pop(context);
                  }
                });
              },
              icon: const Icon(Icons.delete))
        ]),
        body: Column(children: [
          CarouselSlider(
              items: images.map((e) => Image(image: e)).toList(),
              options: CarouselOptions(onPageChanged: (index, reason) {
                setState(() {
                  _indexToShow = index;
                });
              })),
          contentTexts[_indexToShow]
        ], crossAxisAlignment: CrossAxisAlignment.start));
  }

  /// All pieces detected in this analysis
  List<PieceDetection> get _pieces {
    return widget.analyse.ordonatedPieces;
  }
}
