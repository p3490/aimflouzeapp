import 'dart:async';

import 'package:aim_flouze_app/models/image_to_analyse.dart';
import 'package:aim_flouze_app/pages/confirm_upload_page.dart';
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:native_device_orientation/native_device_orientation.dart';
import 'package:uuid/uuid.dart';

/// The page to take coin pictures
class CameraPage extends StatefulWidget {
  const CameraPage({Key? key}) : super(key: key);

  @override
  _CameraPageState createState() => _CameraPageState();
}

/// The Camera Page State
class _CameraPageState extends State<CameraPage> {
  /// The main camera controller
  CameraController? _controller;

  /// The camera used in this widget
  CameraDescription? camera;

  /// The camera preview
  Widget? cameraPreview;

  /// A stream giving the NativeDeviceOrientation each time it changes
  final Stream<NativeDeviceOrientation> _orientationUpdateStream =
      NativeDeviceOrientationCommunicator()
          .onOrientationChanged(useSensor: true);

  /// The orientation update listener
  StreamSubscription? _orientationUpdateListener;

  /// A global key associated to the container with the cameraPreview
  final GlobalKey _cameraKey = GlobalKey();

  /// The position to whitch the camera has to focus
  Offset _focusPosition = const Offset(0.5, 0.5);

  /// Defines camera as the main camera of the widget
  Future<void> definirCamera() async {
    final cameras = await availableCameras();
    if (cameras.isNotEmpty) {
      camera = cameras[0];
    } else {
      camera = null;
    }
  }

  /// Initialize the camera with the parameters of the widget
  Future<void> initCamera() async {
    if (camera != null) {
      _controller =
          CameraController(camera!, ResolutionPreset.max, enableAudio: false);
      await _controller?.initialize();
      await _controller?.setFocusPoint(_focusPosition);
      await _controller?.setFocusMode(FocusMode.auto);
      await _controller?.setFlashMode(FlashMode.off);
      await _controller?.setExposureMode(ExposureMode.auto);
      setState(() {
        cameraPreview = CameraPreview(_controller!);
      });
      await updateCameraOrientation((await _orientationUpdateStream.last));
    }
  }

  @override
  void initState() {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]);
    definirCamera().then((_) {
      initCamera();
    });
    _orientationUpdateListener =
        _orientationUpdateStream.listen(updateCameraOrientation);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Prendre une photo"), actions: [
          IconButton(
              icon: const Icon(Icons.camera_alt),
              onPressed: () async {
                List<ImageToAnalyse> images = [];
                // Takes a picture without the flash
                await _controller?.setFlashMode(FlashMode.off);
                var file = await takePictureToJPEG();
                if (file != null) {
                  images.add(ImageToAnalyse(file, false));
                }

                // Takes a picture with the flash on
                await _controller?.setFlashMode(FlashMode.always);
                file = await takePictureToJPEG();
                if (file != null) {
                  images.add(ImageToAnalyse(file, true));
                }

                // Pass to the next page
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (_) => ConfirmUploadPage(images)));
              })
        ]),
        body: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(),
            GestureDetector(
                child: Container(key: _cameraKey, child: cameraPreview),
                onTapDown: (tapDetails) => onTapDown(context, tapDetails)),
            Container()
          ],
        ));
  }

  /// Takes a picture and save it in the JPEG format
  Future<XFile?> takePictureToJPEG() async {
    String randomId = const Uuid().v1();
    var file = await _controller?.takePicture();
    if (file != null) {
      var path = join((await getTemporaryDirectory()).path, "$randomId.jpg");
      file.saveTo(path);
      return XFile(path);
    }
    return null;
  }

  /// Updates the camera with the position touched as the main focus
  void onTapDown(BuildContext context, TapDownDetails details) {
    final Size size =
        _cameraKey.currentContext?.size ?? MediaQuery.of(context).size;
    _focusPosition = Offset(details.localPosition.dx / size.longestSide,
        details.localPosition.dy / size.shortestSide);
    setState(() {
      cameraPreview = null;
    });
    _controller?.dispose().then((_) async {
      initCamera();
    });
  }

  /// Updates the camera orientation based on a NativeDeviceOrientation
  Future<void> updateCameraOrientation(
      NativeDeviceOrientation orientation) async {
    if (cameraPreview != null) {
      if (orientation == NativeDeviceOrientation.landscapeLeft) {
        await _controller
            ?.lockCaptureOrientation(DeviceOrientation.landscapeLeft);
      } else if (orientation == NativeDeviceOrientation.landscapeRight) {
        await _controller
            ?.lockCaptureOrientation(DeviceOrientation.landscapeRight);
      }
    }
  }

  @override
  void dispose() {
    _controller?.dispose();
    _orientationUpdateListener?.cancel();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }
}
