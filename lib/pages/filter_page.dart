import 'dart:collection';

import 'package:aim_flouze_app/models/filter_parameters.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

/// A page containing parameters to filters submitted analyses
class FilterPage extends StatefulWidget {
  /// The filter parameters set BEFORE using this page
  final FilterParameters filterParameters;

  const FilterPage(this.filterParameters, {Key? key}) : super(key: key);

  @override
  _FilterPageState createState() => _FilterPageState();
}

/// A typedef for functions that returns a FormField based on a BuildContext
typedef _FilterEntry = FormField Function(BuildContext context);

class _FilterPageState extends State<FilterPage> {
  /// The date format used to show dates in this page
  static final DateFormat dateFormat = DateFormat('dd/MM/yyyy');

  /// The date from which the events shall be kept
  late DateTime _fromDate;

  /// A TextEditingController for the "From" widget
  final TextEditingController _fromController = TextEditingController();

  /// The date to which the events shall be kept
  late DateTime _toDate;

  /// A TextEditingController for the "To" widget
  final TextEditingController _toController = TextEditingController();

  /// A Queue containing all the selected widget
  final Queue<_FilterEntry> _selectedWidgetQueue = Queue();

  /// A queue containg all the paramaters widget, associated to a name
  final Queue<MapEntry<String, _FilterEntry>> _allWidgetQueue = Queue();

  /// The date at which this page was created
  final DateTime _nowDate = DateTime.now();

  /// The minimal date for the events
  static final DateTime minimalDate = DateTime.fromMicrosecondsSinceEpoch(0);

  /// A global key associated to the form
  final GlobalKey<FormState> _formKey = GlobalKey();

  final _minValueController = TextEditingController();

  final _maxValueController = TextEditingController();

  @override
  void initState() {
    // Initialisation of all the necessary variables
    _fromDate = widget.filterParameters.fromDate ?? _nowDate;
    _fromController.text = dateFormat.format(_fromDate);
    _toDate = widget.filterParameters.toDate ?? _nowDate;
    _toController.text = dateFormat.format(_toDate);
    _minValueController.text =
        ((widget.filterParameters.minValue ?? 0) / 100).toString();
    _maxValueController.text =
        ((widget.filterParameters.maxValue ?? 0) / 100).toString();
    _allWidgetQueue.addAll([
      MapEntry("De ...", _getFromDateWidget),
      MapEntry("Vers ...", _getToDateWidget),
      MapEntry("Valeur minimale", _getMinValueWidget),
      MapEntry("Valeur maximale", _getMaxValueWidget)
    ]);

    // Add all the filtered that were configured before
    if (widget.filterParameters.fromDate != null) {
      _selectedWidgetQueue.add(_getFromDateWidget);
    }
    if (widget.filterParameters.toDate != null) {
      _selectedWidgetQueue.add(_getToDateWidget);
    }
    if (widget.filterParameters.minValue != null) {
      _selectedWidgetQueue.add(_getMinValueWidget);
    }
    if (widget.filterParameters.maxValue != null) {
      _selectedWidgetQueue.add(_getMaxValueWidget);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Filtrage"), actions: [
          PopupMenuButton<MapEntry<String, _FilterEntry>>(
              // Add only the widget that weren't selected before
              itemBuilder: (context) => _allWidgetQueue
                  .where((element) =>
                      !_selectedWidgetQueue.contains(element.value))
                  .map((e) => PopupMenuItem(child: Text(e.key), value: e))
                  .toList(),
              icon: const Icon(Icons.add),
              onSelected: (entry) {
                setState(() {
                  _selectedWidgetQueue.addFirst(entry.value);
                });
              }),
          IconButton(onPressed: validate, icon: const Icon(Icons.check))
        ]),
        body: Form(
            key: _formKey,
            child: Column(
                children:
                    // Add all widget that have been selected before
                    _selectedWidgetQueue.map((e) => e(context)).toList())));
  }

  /// Returns a widget that configures a maximal date filter
  TextFormField _getToDateWidget(BuildContext context) {
    // This TextFormField is set as read-only.
    // This form value is updated with a datePicker pop-up shown when the
    // user click on the form.
    return TextFormField(
      readOnly: true,
      controller: _toController,
      decoration: InputDecoration(
          labelText: "Date de fin (exclusive)",
          prefixIcon: const Icon(Icons.date_range),
          suffixIcon: IconButton(
              onPressed: () {
                setState(() {
                  _selectedWidgetQueue.remove(_getToDateWidget);
                });
              },
              icon: const Icon(Icons.remove))),
      onTap: () {
        showDatePicker(
                context: context,
                initialDate: _toDate,
                firstDate: minimalDate,
                lastDate: _nowDate)
            .then((date) {
          if (date != null) {
            setState(() {
              _toDate = date;
              _toController.text = dateFormat.format(date);
            });
          }
        });
      },
      autovalidateMode: AutovalidateMode.always,
      validator: (value) {
        if (value != null) {
          if (_selectedWidgetQueue.contains(_getFromDateWidget) &&
              _fromDate.isAfter(dateFormat.parse(value))) {
            return "La date de début est après la date de fin";
          }
        }
        return null;
      },
    );
  }

  /// Returns a widget that configures a minimal date filter
  TextFormField _getFromDateWidget(BuildContext context) {
    // This TextFormField is set as read-only.
    // This form value is updated with a datePicker pop-up shown when the
    // user click on the form.
    return TextFormField(
        readOnly: true,
        controller: _fromController,
        decoration: InputDecoration(
            labelText: "Date de début (inclusive)",
            prefixIcon: const Icon(Icons.date_range),
            suffixIcon: IconButton(
                onPressed: () {
                  setState(() {
                    _selectedWidgetQueue.remove(_getFromDateWidget);
                  });
                },
                icon: const Icon(Icons.remove))),
        onTap: () {
          showDatePicker(
                  context: context,
                  initialDate: _fromDate,
                  firstDate: minimalDate,
                  lastDate: _nowDate)
              .then((date) {
            if (date != null) {
              setState(() {
                _fromDate = date;
                _fromController.text = dateFormat.format(date);
              });
            }
          });
        },
        autovalidateMode: AutovalidateMode.always,
        validator: (value) {
          if (value != null) {
            if (_selectedWidgetQueue.contains(_getToDateWidget) &&
                _toDate.isBefore(dateFormat.parse(value))) {
              return "La date de fin est avant la date de début";
            }
          }
          return null;
        });
  }

  /// Returns a widget that configures a minimum value filter
  FormField _getMinValueWidget(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
          labelText: "Valeur minimale (euros)",
          prefixIcon: const Icon(Icons.compare_arrows),
          suffixIcon: IconButton(
              onPressed: () {
                setState(() {
                  _selectedWidgetQueue.remove(_getMinValueWidget);
                });
              },
              icon: const Icon(Icons.remove))),
      controller: _minValueController,
      autovalidateMode: AutovalidateMode.always,
      keyboardType:
          const TextInputType.numberWithOptions(signed: false, decimal: true),
      validator: (value) {
        final doubleValue = double.tryParse(value ?? "A");
        if (doubleValue == null) {
          return "Pas de valeur";
        } else if (doubleValue >
                (double.tryParse(_maxValueController.text) ?? doubleValue) &&
            _selectedWidgetQueue.contains(_getMaxValueWidget)) {
          return "La valeur maximale est après la valeur minimale";
        }
        return null;
      },
    );
  }

  /// Returns a widget that configures a maximum value filter
  FormField _getMaxValueWidget(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
          labelText: "Valeur maximale (euros)",
          prefixIcon: const Icon(Icons.compare_arrows),
          suffixIcon: IconButton(
              onPressed: () {
                setState(() {
                  _selectedWidgetQueue.remove(_getMaxValueWidget);
                });
              },
              icon: const Icon(Icons.remove))),
      controller: _maxValueController,
      autovalidateMode: AutovalidateMode.always,
      keyboardType:
          const TextInputType.numberWithOptions(signed: false, decimal: true),
      validator: (value) {
        final doubleValue = double.tryParse(value ?? "A");
        if (doubleValue == null) {
          return "Pas de valeur";
        } else if (doubleValue <
                (double.tryParse(_maxValueController.text) ?? doubleValue) &&
            _selectedWidgetQueue.contains(_getMinValueWidget)) {
          return "La valeur minimale est avant la valeur maximale";
        }
        return null;
      },
    );
  }

  /// Validates the filter parameters and go back to the previous page
  /// returning the new FilterParameters
  void validate() {
    if (_formKey.currentState?.validate() ?? false) {
      Navigator.pop(
          context,
          FilterParameters(
            _selectedWidgetQueue.contains(_getFromDateWidget)
                ? _fromDate
                : null,
            _selectedWidgetQueue.contains(_getToDateWidget) ? _toDate : null,
            _selectedWidgetQueue.contains(_getMinValueWidget)
                ? double.parse(_minValueController.text) * 100
                : null,
            _selectedWidgetQueue.contains(_getMaxValueWidget)
                ? double.parse(_maxValueController.text) * 100
                : null,
          ));
    }
  }
}
