import 'package:aim_flouze_app/models/analyse_submission.dart';
import 'package:aim_flouze_app/utils/rest_utils.dart';
import 'package:flutter/material.dart';

/// A widget to upload an analyse
class UploadPage extends StatefulWidget {
  /// The analyse to uplaod
  final AnalyseSubmission analyse;

  const UploadPage(this.analyse, {Key? key}) : super(key: key);

  @override
  _UploadPageState createState() => _UploadPageState();
}

class _UploadPageState extends State<UploadPage> {
  /// Has the upload been finished?
  bool _uploadFinished = false;

  /// Has the upload been succesfull?
  bool _uploadSuceeded = false;

  @override
  void initState() {
    RESTUtils().submitAnalyse(widget.analyse).then((response) {
      setState(() {
        _uploadFinished = true;
        _uploadSuceeded = response.responseStatus == RESTResponseStatus.ok;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Center(child: Text("Envoi des images")),
          leading: (_uploadFinished
              ? IconButton(
                  icon: const Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.popUntil(context, ModalRoute.withName("/home"));
                  })
              : null),
          automaticallyImplyLeading: false,
        ),
        body: Builder(builder: (context) {
          if (!_uploadFinished) {
            return const CircularProgressIndicator(value: null);
          } else if (_uploadSuceeded) {
            return const Text("Upload reussi");
          } else {
            return const Text("Upload raté");
          }
        }));
  }
}
