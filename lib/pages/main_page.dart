import 'dart:async';

import 'package:aim_flouze_app/dialogues/confirm_dialogue.dart';
import 'package:aim_flouze_app/dialogues/server_parameter_dialog.dart';
import 'package:aim_flouze_app/dialogues/user_parameter_dialogue.dart';
import 'package:aim_flouze_app/models/analyse_submission.dart';
import 'package:aim_flouze_app/models/filter_parameters.dart';
import 'package:aim_flouze_app/models/image_to_analyse.dart';
import 'package:aim_flouze_app/models/sort_algorithms.dart';
import 'package:aim_flouze_app/models/submitted_analyse.dart';
import 'package:aim_flouze_app/models/user_informations.dart';
import 'package:aim_flouze_app/pages/camera_page.dart';
import 'package:aim_flouze_app/pages/filter_page.dart';
import 'package:aim_flouze_app/pages/upload_page.dart';
import 'package:aim_flouze_app/utils/rest_utils.dart';
import 'package:aim_flouze_app/widgets/analyse_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:image_picker/image_picker.dart';

/// The Main Page, the one with all the analyses submitted
class MainPage extends StatefulWidget {
  /// A list of SortOptions that can be used in this page
  final List<SortOption<SubmittedAnalyse>> sortOptions;

  const MainPage({Key? key, this.sortOptions = const []}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

typedef _ContextFunction = void Function(BuildContext context);

class _MainPageState extends State<MainPage> {
  /// The RESTUtils used to get all the informations
  static final RESTUtils _restUtils = RESTUtils();

  /// An iterable of SubmittedAnalyse shown in this page
  Iterable<SubmittedAnalyse> _analyses = [];

  /// A StreamController that will contain all the RESTResponses with
  /// an error
  final StreamController<RESTResponse> _errorStreamController =
      StreamController();

  /// The stream containing all the "not-ok" RESTResponses
  Stream<RESTResponse>? _errorsStream;

  /// A list of filter that will be used in this page
  FilterParameters _filterParameters = const FilterParameters.empty();

  /// The sort option used in this page for the submitted analyses
  SortOption<SubmittedAnalyse>? _sortSelected;

  /// The user informations
  UserInformations? userInfo;

  @override
  void initState() {
    // Initialisation of the errorsStream
    _errorsStream = _errorStreamController.stream;
    _errorsStream?.listen((event) async {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Erreur : ${event.responseStatus.description}")));
    });

    // Reloads the user informations right at the begining of the page
    userInfo = _restUtils.userInfos;
    updateAll();

    _restUtils.userStream.map((element) {
      if (element != null) {
        updateAll();
      }
    });

    super.initState();
  }

  /// Update every informations set in this page
  Future<void> updateAll() async {
    var response = await _restUtils.reloadUserInformations();
    if (response.isOk) {
      setState(() {
        userInfo = response.content;
      });
      refreshAnalyseList();
    } else {
      _errorStreamController.add(response);
    }
  }

  @override
  void dispose() {
    _errorStreamController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title:
                Center(child: Text("Bonjour ${userInfo?.firstName ?? "..."}")),
            actions: [_filterPageButton, _sortButton, _moreOptionWidget]),
        body: _analyseListWidget,
        floatingActionButton: _floatingActionButton);
  }

  /// Returns the floating action button that will be shown at the bottom-right
  /// corner of the screen
  Widget get _floatingActionButton {
    return SpeedDial(
        icon: Icons.add,
        activeIcon: Icons.close,
        spacing: 3,
        childPadding: const EdgeInsets.all(5),
        spaceBetweenChildren: 4,
        children: [
          SpeedDialChild(
              child: const Icon(Icons.add_a_photo),
              label: "Photo",
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (_) => const CameraPage()));
              }),
          SpeedDialChild(
              child: const Icon(Icons.add_photo_alternate),
              label: "Galerie",
              onTap: () async {
                ImagePicker picker = ImagePicker();
                final XFile? photo =
                    await picker.pickImage(source: ImageSource.gallery);
                if (photo != null) {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => UploadPage(AnalyseSubmission(
                              [ImageToAnalyse(photo, false)],
                              DateTime.now()))));
                }
              })
        ]);
  }

  /// Returns a button to go to the filter setting page
  IconButton get _filterPageButton {
    return IconButton(
        onPressed: () {
          Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => FilterPage(_filterParameters)))
              .then((value) => {
                    if (value != null)
                      {
                        setState(() {
                          _filterParameters = value;
                        })
                      }
                  });
        },
        icon: const Icon(Icons.filter_alt));
  }

  /// Returns a PopupMenuButton to configure the sorting of the list
  PopupMenuButton<SortOption<SubmittedAnalyse>> get _sortButton {
    return PopupMenuButton<SortOption<SubmittedAnalyse>>(
        itemBuilder: (context) => widget.sortOptions
            .map((e) => PopupMenuItem(child: Text(e.name), value: e))
            .toList(),
        onSelected: (value) {
          setState(() {
            _sortSelected = value;
          });
        },
        icon: const Icon(Icons.sort));
  }

  /// Returns a Widget with a list of all analyses submitted
  AnalyseList get _analyseListWidget {
    List<SubmittedAnalyse> list =
        _analyses.where(_filterParameters.shouldBeKept).toList();
    list.sort(_sortSelected?.sortAlgorithm ??
        (a, b) => -a.dateOfPost.compareTo(b.dateOfPost));
    return AnalyseList(list, () async {
      await updateAll();
    });
  }

  /// Returns a PopupMenuButton containing more options
  PopupMenuButton<_ContextFunction> get _moreOptionWidget {
    return PopupMenuButton<_ContextFunction>(
        onSelected: (function) {
          function(context);
        },
        itemBuilder: (context) => [
              PopupMenuItem(
                  child: const Text("Serveur REST"),
                  value: (context) {
                    showDialog(
                            context: context,
                            builder: (context) => const ServerParameterDialog())
                        .then((_) {
                      setState(() {});
                    }).then((_) async {
                      var response = await _restUtils.reloadUserInformations();
                      if (response.responseStatus == RESTResponseStatus.ok) {
                        setState(() {});
                        await refreshAnalyseList();
                      } else {
                        _errorStreamController.add(response);
                      }
                    });
                  }),
              PopupMenuItem(
                  child: const Text("Mon compte"),
                  value: (context) {
                    showDialog(
                            context: context,
                            builder: (context) =>
                                UsersParametersDialog(_restUtils.userInfos))
                        .then((_) {
                      setState(() {});
                    });
                  }),
              PopupMenuItem(
                  child: const Text("Se déconnecter"),
                  value: (context) {
                    showDialog(
                        context: context,
                        builder: (context) => ConfirmDialog(
                                "Voulez-vous vous déconnecter?", () async {
                              await _restUtils.signOut();
                              return true;
                            })).then((_) async {
                      if (_ == true) {
                        Navigator.popAndPushNamed(context, "/login");
                      }
                    });
                  })
            ]);
  }

  /// Refresh the list of SubmittedAnalyse
  Future<void> refreshAnalyseList() async {
    final response = await _restUtils.getAllAnalyses();
    if (response.responseStatus == RESTResponseStatus.ok) {
      var content = response.content;
      setState(() {
        _analyses = content ?? [];
      });
    } else {
      _errorStreamController.add(response);
    }
  }
}
