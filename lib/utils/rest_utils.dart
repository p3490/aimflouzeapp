import 'dart:io';

import 'package:aim_flouze_app/models/analyse_submission.dart';
import 'package:aim_flouze_app/models/submitted_analyse.dart';
import 'package:aim_flouze_app/models/user_informations.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

/// An enumeration contaning all the types of possible REST Response Status
enum RESTResponseStatus {
  /// Everything went allright
  ok,

  /// The ressource was not found
  notFound,

  /// You are not authorized to get this ressource
  unauthorized,

  /// There was an error from the server
  serverError,

  /// Unknown error
  unknown,

  /// The REST Server is unjoinable
  socketError
}

/// Returns a RESTResponseStatus based on the HTTP Status code
RESTResponseStatus restResponseStatusFromHTTPStatus(int status) {
  switch (status) {
    case 200:
      return RESTResponseStatus.ok;
    case 404:
      return RESTResponseStatus.notFound;
    case 401:
      return RESTResponseStatus.unauthorized;
    case 500:
      return RESTResponseStatus.serverError;
  }
  return RESTResponseStatus.unknown;
}

/// An extension to add some methods to the enum
extension RESTResponsStatusExtension on RESTResponseStatus {
  String get description {
    switch (this) {
      case RESTResponseStatus.ok:
        return "OK";
      case RESTResponseStatus.notFound:
        return "Page non trouvé";
      case RESTResponseStatus.serverError:
        return "Erreur du serveur";
      case RESTResponseStatus.socketError:
        return "Erreur du socket";
      case RESTResponseStatus.unauthorized:
        return "Non autorisé";
      case RESTResponseStatus.unknown:
        return "Erreur inconnu";
    }
  }
}

/// A class containing an responseStatus, and the content of the response
class RESTResponse<T> {
  /// The status of the response
  final RESTResponseStatus responseStatus;

  /// The content of the response
  final T? content;

  const RESTResponse(this.responseStatus, this.content);

  /// Creates a RESTResponse that has succeded
  const RESTResponse.ok(this.content) : responseStatus = RESTResponseStatus.ok;

  /// Creates a RESTResponse from a HTTP Status Code, and a content
  factory RESTResponse.fromStatusCode(int statusCode, {T? content}) {
    return RESTResponse(restResponseStatusFromHTTPStatus(statusCode), content);
  }

  /// Returns a default response when there is a socket error
  const RESTResponse.socketError() : this(RESTResponseStatus.socketError, null);

  /// Returns a default response when there is a unknown error
  const RESTResponse.unknowError() : this(RESTResponseStatus.unknown, null);

  /// Return true if the request succeded, false otherwise
  bool get isOk {
    return responseStatus == RESTResponseStatus.ok;
  }
}

/// An util class related to RestAPI changes
class RESTUtils {
  /// The based authentification tool given by Firebase
  static final FirebaseAuth _authentificationFirebase = FirebaseAuth.instance;

  /// The only instance of RESTUtils. We use here the singleton design
  /// pattern
  static final RESTUtils _restUtils = RESTUtils._internal();

  /// The SharedPreferences instance
  SharedPreferences? preferences;

  /// The user informations
  UserInformations? userInfos;

  /// The key in the SharedPreferences instances associated to the REST server
  /// IP
  static const String SERVER_IP_KEY = "SERVER_IP";

  /// The default server if no server were registered
  static const String DEFAULT_SERVER_IP = "192.168.0.20";

  /// The key in the SharedPreferences instances associated to the REST server
  /// port
  static const String PORT_KEY = "SERVER_PORT";

  /// The default port if no port were registered
  static const String DEFAULT_PORT = "5000";

  /// The internal and private constructor, launched one time only
  RESTUtils._internal() {
    SharedPreferences.getInstance().then((preferencesInstance) async {
      String? ip = preferencesInstance.getString(SERVER_IP_KEY);
      if (ip == null) {
        await preferencesInstance.setString(SERVER_IP_KEY, DEFAULT_SERVER_IP);
      }
      String? port = preferencesInstance.getString(PORT_KEY);
      if (port == null) {
        await preferencesInstance.setString(PORT_KEY, DEFAULT_PORT);
      }
      preferences = preferencesInstance;
    });
    reloadUserInformations();
  }

  /// The main factory that gives every
  /// time the same instance of Authentification
  factory RESTUtils() {
    return _restUtils;
  }

  /// Sign in with the given email and password, return the user ID.
  /// This methods can throw an [FirebaseAuthException]
  Future<String?> signIn(String email, String password) async {
    UserCredential credential = await _authentificationFirebase
        .signInWithEmailAndPassword(email: email, password: password);
    return credential.user?.uid;
  }

  /// Create an account with the given email and password, return the user ID.
  /// This methods can throw an [FirebaseAuthException]
  Future<RESTResponse<String>> createAccount(
      String email, String password) async {
    UserCredential credential = await _authentificationFirebase
        .createUserWithEmailAndPassword(email: email, password: password);
    await _authentificationFirebase.signInWithEmailAndPassword(
        email: email, password: password);
    var response = await createEmptyAccount();
    return RESTResponse(response.responseStatus, credential.user?.uid);
  }

  /// Sign out the current user
  Future signOut() async {
    await _authentificationFirebase.signOut();
    userInfos = null;
  }

  /// Retourne un objet contenant des informations sur l'utilisateur
  User? get firebaseUser {
    return _authentificationFirebase.currentUser;
  }

  /// Returns a stream with the FireBase user
  Stream<User?> get userStream {
    return _authentificationFirebase.userChanges();
  }

  // Return an JWT FireAuth token for the current user
  Future<String?> getToken() async {
    return firebaseUser?.getIdToken();
  }

  /// The server IP/name
  String get serverName {
    return preferences?.getString(SERVER_IP_KEY) ?? DEFAULT_SERVER_IP;
  }

  set serverName(String name) {
    preferences?.setString(SERVER_IP_KEY, name);
  }

  /// The server IP/name
  String get serverPort {
    return preferences?.getString(PORT_KEY) ?? DEFAULT_PORT;
  }

  set serverPort(String name) {
    preferences?.setString(PORT_KEY, name);
  }

  /// Returns the default headers used in most of the HTTP requests
  Future<Map<String, String>> getDefaultHeaders() async {
    String? token = await getToken();
    if (token != null) {
      return {"x-access-token": token};
    } else {
      return const {};
    }
  }

  /// Create an empty account document on firestore with no first and last name.
  /// Returns if the account was sucesfully created or not
  Future<RESTResponse<void>> createEmptyAccount() async {
    if ((await getToken()) != null) {
      try {
        http.Response response = await http.put(
            Uri.parse("http://$serverName:$serverPort/createEmptyAccount"),
            headers: await getDefaultHeaders());
        return RESTResponse.fromStatusCode(response.statusCode);
      } on SocketException {
        return const RESTResponse.socketError();
      }
    }
    return const RESTResponse.unknowError();
  }

  /// Update the user informations and return it to the user
  Future<RESTResponse<UserInformations>> reloadUserInformations() async {
    if ((await getToken()) != null) {
      try {
        http.Response response = await http.get(
            Uri.parse("http://$serverName:$serverPort/account/me"),
            headers: await getDefaultHeaders());
        if (response.statusCode == 200) {
          Map<String, dynamic> json = jsonDecode(response.body);
          userInfos = UserInformations.fromJson(json);
          return RESTResponse.ok(userInfos);
        } else {
          return RESTResponse.fromStatusCode(response.statusCode);
        }
      } on SocketException {
        return const RESTResponse.socketError();
      }
    }
    return const RESTResponse.unknowError();
  }

  /// Update the user informations with the informations given in [infos]
  Future<RESTResponse<void>> updateUserInformation(
      UserInformations infos) async {
    final token = await getToken();
    if (token != null) {
      try {
        http.Response response = await http.post(
            Uri.parse("http://$serverName:$serverPort/account/me"),
            headers: {
              "x-access-token": token,
              "Content-Type": "application/json"
            },
            body: jsonEncode(infos.toJson()));
        if (response.statusCode == 200) {
          userInfos = infos;
          return const RESTResponse.ok(null);
        } else {
          return RESTResponse.fromStatusCode(response.statusCode);
        }
      } on SocketException {
        return const RESTResponse.socketError();
      }
    }
    return const RESTResponse.unknowError();
  }

  /// Submit an new analyse to the REST Server with the informations given in [submission]
  Future<RESTResponse<void>> submitAnalyse(AnalyseSubmission submission) async {
    final token = await getToken();
    if (token != null) {
      try {
        http.Response response = await http.post(
            Uri.parse("http://$serverName:$serverPort/analyse"),
            headers: {
              "x-access-token": token,
              "Content-Type": "application/json"
            },
            body: json.encode(await submission.toJson()));
        if (response.statusCode == 200) {
          return const RESTResponse.ok(null);
        } else {
          return RESTResponse.fromStatusCode(response.statusCode);
        }
      } on SocketException {
        return const RESTResponse.socketError();
      }
    }
    return const RESTResponse.unknowError();
  }

  /// Return all the analyses submitted by the connected user
  Future<RESTResponse<List<SubmittedAnalyse>>> getAllAnalyses() async {
    final token = await getToken();
    if (token != null) {
      try {
        http.Response response = await http.get(
            Uri.parse("http://$serverName:$serverPort/analyse"),
            headers: await getDefaultHeaders());
        if (response.statusCode == 200) {
          List<dynamic> content = json.decode(response.body);
          return RESTResponse.ok(
              content.map((e) => SubmittedAnalyse.fromJson(e)).toList());
        } else {
          return RESTResponse.fromStatusCode(response.statusCode);
        }
      } on SocketException {
        return const RESTResponse.socketError();
      }
    }
    return const RESTResponse.unknowError();
  }

  /// Delete an analyse based on his uuid
  Future<RESTResponse<void>> deleteAnalyse(String uuid) async {
    final token = await getToken();
    if (token != null) {
      try {
        http.Response response = await http.delete(
            Uri.parse("http://$serverName:$serverPort/analyse/$uuid"),
            headers: await getDefaultHeaders());
        if (response.statusCode == 200) {
          return const RESTResponse.ok(null);
        } else {
          return RESTResponse.fromStatusCode(response.statusCode);
        }
      } on SocketException {
        return const RESTResponse.socketError();
      }
    }
    return const RESTResponse.unknowError();
  }

  /// Delete an account
  Future<RESTResponse<bool>> deleteAccount(
      String email, String password) async {
    final token = await getToken();
    if (token != null) {
      try {
        http.Response response = await http.delete(
            Uri.parse("http://$serverName:$serverPort/account/me"),
            headers: await getDefaultHeaders());
        if (response.statusCode == 200) {
          await _authentificationFirebase.currentUser
              ?.reauthenticateWithCredential(EmailAuthProvider.credential(
                  email: email, password: password));
          await _authentificationFirebase.currentUser?.delete();
          return const RESTResponse.ok(true);
        } else {
          return RESTResponse.fromStatusCode(response.statusCode);
        }
      } on SocketException {
        return const RESTResponse.socketError();
      }
    }
    return const RESTResponse.unknowError();
  }
}
